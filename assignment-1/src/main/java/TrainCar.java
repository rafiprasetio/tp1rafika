public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms

    // TODO Complete me!
    WildCat cat;
    TrainCar next;

    public TrainCar(WildCat cat) {
        // TODO Complete me!
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        // TODO Complete me!
        this.cat = cat;
        this.next = next;
    }

    public double computeTotalWeight() {
        return computeTotalWeight(next, cat);
    }

    /*
    Recursive Method for Computing Total Weight
     */
    public static double computeTotalWeight(TrainCar next, WildCat cat) {
        // TODO Complete me!
        if (next == null) {
            return EMPTY_WEIGHT + cat.weight;
        } else {
            return EMPTY_WEIGHT + cat.weight + computeTotalWeight(next.next, next.cat);
        }
    }

    public double computeTotalMassIndex() {
        return computeTotalMassIndex(next, cat);
    }

    /*
    Recursive Method for Computing Mass Index
     */
    public static double computeTotalMassIndex(TrainCar next, WildCat cat) {
        // TODO Complete me!
        if (next == null) {
            return cat.computeMassIndex();
        } else {
            return cat.computeMassIndex() + computeTotalMassIndex(next.next, next.cat);
        }
    }

    public void printCar() {
        System.out.println(printCar(next, cat));
    }

    public static String printCar(TrainCar next, WildCat cat) {
        // TODO Complete me!
        if (next == null) {
            return "(" + cat.name + ")";
        } else {
            return "(" + cat.name + ")--" + printCar(next.next, next.cat);
        }
    }

    public int getLength() {
        int lengthResult = 1;
        TrainCar pointer = this;
        while (pointer.next != null) {
            lengthResult += 1;
            pointer = pointer.next;
        }
        return lengthResult;
    }
}
