import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    // You can add new variables or methods in this class

    public static void main(String[] args) {
        // TODO Complete me!
        Scanner sc = new Scanner(System.in);
        int jumlahKucing = sc.nextInt();

        sc.nextLine();
        String input;
        TrainCar head = null; //Gerbong paling depan awalnya null sebelum di deklarasi

        for (int i = 0; i < jumlahKucing; i++) {
            input = sc.nextLine();
            String[] inp_split = input.split(",");

            String name = inp_split[0]; //Nama kucingnya
            double weight = Double.parseDouble(inp_split[1]); //Berat kucingnya
            int length = Integer.parseInt(inp_split[2]); //Panjang kucingnya

            WildCat cat = new WildCat(name, weight, length);
            if (head == null) {
                head = new TrainCar(cat);
            }
            else {
                TrainCar temp = new TrainCar(cat);
                if (head.computeTotalWeight() > THRESHOLD) {
                    departTrain(head);
                    head = temp;
                }
                else {
                    temp.next = head;
                    head = temp;
                }
            }
        }
        if (head != null) {
            departTrain(head);
        }
    }

    public static void departTrain(TrainCar head) {
        System.out.println("The train departs to Javari Park");
        System.out.print("[LOCO]<--");
        head.printCar();

        double totalMassIndex = head.computeTotalMassIndex();
        double averageMassIndex = totalMassIndex / head.getLength();

        System.out.printf("Average mass index of all cats: %.2f \n",averageMassIndex);
        System.out.print("In average, the cats in the train are ");

        if (averageMassIndex < 18.5) {
            System.out.println("*underweight*");
        }
        else if (averageMassIndex >= 18.5 && averageMassIndex < 25) {
            System.out.println("*normal*");
        }
        else if (averageMassIndex >= 30) {
            System.out.println("*obese*");
        }
        else if (averageMassIndex >= 25 && averageMassIndex < 30) {
            System.out.println("*overweight*");
        }
    }
}